package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;

public class UserLogoutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "logout";
    }

    @Override
    @NotNull
    public String description() {
        return "Log out.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        @NotNull final Result result =  serviceLocator.getSessionEndpoint().closeSession(serviceLocator.getSession());
        printResult(result);
        serviceLocator.setSession(null);
    }

}
