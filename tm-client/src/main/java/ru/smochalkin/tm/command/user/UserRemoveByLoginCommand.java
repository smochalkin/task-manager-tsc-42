package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand; 
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "User removing by login.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final Result result = serviceLocator.getAdminEndpoint().removeUserByLogin(serviceLocator.getSession(), login);
        printResult(result);
    }

}
