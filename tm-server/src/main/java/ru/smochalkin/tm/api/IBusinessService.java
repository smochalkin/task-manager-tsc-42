package ru.smochalkin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.AbstractBusinessEntityDto;

import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntityDto> extends IService<E> {

    void create(@NotNull String userId, @Nullable String name, @Nullable String description);

    void clear(String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull String sort);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findById(@NotNull String userId, @Nullable String id);

    @Nullable
    E findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    E findByIndex(@NotNull String userId, @NotNull Integer index);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeByName(@NotNull String userId, @NotNull String name);

    void removeByIndex(@NotNull String userId, @NotNull Integer index);

    void updateById(@NotNull String id, @NotNull String name, @Nullable String desc);

    void updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @Nullable String desc);

    void updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @Nullable String desc);

    void updateStatusById(@NotNull String userId, @NotNull String id, @NotNull String status);

    void updateStatusByName(@NotNull String userId, @NotNull String name, @NotNull String status);

    void updateStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String status);

    boolean isNotIndex(@NotNull String userId, @Nullable Integer index);

}
