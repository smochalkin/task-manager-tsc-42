package ru.smochalkin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.dto.ProjectDto;

import java.util.Date;
import java.util.List;

public interface IProjectRepository {

    @Delete("DELETE FROM tm_project")
    void clear();

    @NotNull
    @Select("SELECT id, name, description, status, created, start_date, end_date, user_id FROM tm_project")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDto> findAll();

    @NotNull
    @Select("SELECT id, name, description, status, created, start_date, end_date, user_id " +
            "FROM tm_project WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "userId", column = "user_id")
    })
    ProjectDto findById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void removeById(@Param("id") @NotNull String id);

    @Select("SELECT count(*) FROM tm_project")
    int getCount();

    @Insert("INSERT INTO tm_project (id, name, description, status, user_id)" +
            " VALUES (#{id}, #{name}, #{description}, #{status}, #{userId})")
    void add(
            @Param("id") @NotNull String id,
            @Param("name") @NotNull String name,
            @Param("description") @Nullable String description,
            @Param("status") @NotNull String status,
            @Param("userId") @NotNull String userId
    );

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @NotNull String userId);

    @NotNull
    @Select("SELECT id, name, description, status, created, start_date, end_date, user_id " +
            "FROM tm_project WHERE user_id = #{userId} ORDER BY #{sort}")
    List<ProjectDto> findAllByUserIdSorted(@NotNull String userId, @NotNull final String sort);

    @NotNull
    @Select("SELECT id, name, description, status, created, start_date, end_date, user_id " +
            "FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDto> findAllByUserId(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT id, name, description, status, created, start_date, end_date, user_id " +
            "FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "userId", column = "user_id")
    })
    ProjectDto findByUserIdAndId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT id, name, description, status, created, start_date, end_date, user_id " +
            "FROM tm_project WHERE name = #{name} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "userId", column = "user_id")
    })
    ProjectDto findByName(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name);

    @Nullable
    @Select("SELECT id, name, description, status, created, start_date, end_date, user_id " +
            "FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "userId", column = "user_id")
    })
    ProjectDto findByIndex(@Param("userId") @NotNull String userId, @Param("index") int index);

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    void removeByUserIdAndId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_project WHERE name = #{name} AND user_id = #{userId}")
    void removeByName(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name);

    @Delete("DELETE FROM tm_project WHERE id = (SELECT id FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void removeByIndex(@Param("userId") @NotNull String userId, @Param("index") @NotNull Integer index);

    @Update("UPDATE tm_project SET name = #{name}, description = #{desc} WHERE id = #{id}")
    void updateById(@Param("id") @NotNull String id, @Param("name") @NotNull String name, @Param("desc") @Nullable String desc);

    @Update("UPDATE tm_project SET name = #{name}, description = #{desc} WHERE id = #{id} AND user_id = #{userId}")
    void updateByUserIdAndId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id,
            @Param("name") @NotNull String name,
            @Param("desc") @Nullable String desc
    );

    @Delete("UPDATE tm_project SET name = #{name}, description = #{desc} WHERE id = " +
            "(SELECT id FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void updateByIndex(
            @Param("userId") @NotNull String userId,
            @Param("index") @NotNull Integer index,
            @Param("name") @NotNull String name,
            @Param("desc") @Nullable String desc
    );

    @Update("UPDATE tm_project SET status = #{status}, start_date = #{startDate}, end_date = #{endDate}" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void updateStatusById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id,
            @Param("status") @NotNull Status status,
            @Param("startDate") @NotNull Date startDate,
            @Param("endDate") @NotNull Date endDate
    );

    @Update("UPDATE tm_project SET status = #{status}, start_date = #{startDate}, end_date = #{endDate}" +
            " WHERE user_id = #{userId} AND name = #{name}")
    void updateStatusByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name,
            @Param("status") @NotNull Status status,
            @Param("startDate") @NotNull Date startDate,
            @Param("endDate") @NotNull Date endDate
    );

    @Update("UPDATE tm_project SET status = #{status}, start_date = #{startDate}, end_date = #{endDate}" +
            " WHERE id = (SELECT id FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void updateStatusByIndex(
            @Param("userId") @NotNull String userId,
            @Param("index") @NotNull Integer index,
            @Param("status") @NotNull Status status,
            @Param("startDate") @NotNull Date startDate,
            @Param("endDate") @NotNull Date endDate
    );

    @Select("SELECT count(*) FROM tm_project WHERE user_id = #{userId}")
    int getCountByUser(@Param("userId") @NotNull String userId);

}
