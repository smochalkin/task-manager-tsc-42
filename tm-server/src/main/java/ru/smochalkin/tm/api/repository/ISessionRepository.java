package ru.smochalkin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.SessionDto;

import java.util.List;

public interface ISessionRepository {

    @Delete("DELETE FROM tm_session")
    void clear();

    @NotNull
    @Select("SELECT * FROM tm_session")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "time_stamp")
    })
    List<SessionDto> findAll();

    @NotNull
    @Select("SELECT * FROM tm_session WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "time_stamp")
    })
    SessionDto findById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void removeById(@Param("id") @NotNull String id);

    @Select("SELECT count(*) FROM tm_session")
    int getCount();

    @Insert("INSERT INTO tm_session (id, user_id, signature, time_stamp) VALUES (#{id}, #{userId}, #{signature}, #{timestamp})")
    void add(@Param("id") @NotNull String id,
             @Param("userId") @NotNull String userId,
             @Param("signature") @Nullable String signature,
             @Param("timestamp") long timestamp);

    @NotNull
    @Select("SELECT * FROM tm_session WHERE user_id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "time_stamp")
    })
    List<SessionDto> findAllByUserId(@Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void removeByUserId(@Param("userId") @NotNull String userId);

}
