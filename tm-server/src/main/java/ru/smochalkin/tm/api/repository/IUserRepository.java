package ru.smochalkin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.enumerated.Role;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password_hash, email, role) " +
            "VALUES(#{id}, #{login}, #{password}, #{email}, #{role})")
    void add(@Param("id") @NotNull String id,
             @Param("login") @NotNull String login,
             @Param("password") @NotNull String password,
             @Param("email") @NotNull String email,
             @Param("role") @NotNull Role role);

    @Delete("DELETE FROM tm_user")
    void clear();

    @NotNull
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "email", column = "email"),
            @Result(property = "role", column = "role"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "lock", column = "lock")
    })
    List<UserDto> findAll();

    @NotNull
    @Select("SELECT * FROM tm_user WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "email", column = "email"),
            @Result(property = "role", column = "role"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "lock", column = "lock")
    })
    UserDto findById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE LOGIN = #{login}")
    @Result(property = "id", column = "id")
    @Result(property = "login", column = "login")
    @Result(property = "passwordHash", column = "password_hash")
    @Result(property = "email", column = "email")
    @Result(property = "role", column = "role")
    @Result(property = "firstName", column = "first_name")
    @Result(property = "middleName", column = "middle_name")
    @Result(property = "lastName", column = "last_name")
    @Result(property = "lock", column = "lock")
    UserDto findByLogin(@Param("login") @NotNull String login);

    @Delete("DELETE FROM tm_user WHERE ID = #{id}")
    void removeById(@Param("id") @NotNull String id);

    @Update("UPDATE tm_user SET first_name = #{firstName}, middle_name = #{middleName}, last_name = #{lastName} " +
            "WHERE id = #{id}")
    void updateById(
            @Param("id") @Nullable String id,
            @Param("firstName") @Nullable String firstName,
            @Param("lastName") @Nullable String lastName,
            @Param("middleName") @Nullable String middleName
    );

    @Update("UPDATE tm_user SET password_hash = #{password} WHERE id = #{id}")
    void updatePasswordById(@Param("id") @NotNull String id, @Param("password") @NotNull String password);

    @Update("UPDATE tm_user SET lock = #{lock} WHERE login = #{login}")
    void updateLockByLogin(@Param("login") @NotNull String login, @Param("lock") boolean lock);

    @Delete("DELETE FROM tm_user WHERE login = #{login}")
    void removeByLogin(@Param("login") @NotNull String login);

    @Select("SELECT count(*) FROM tm_user")
    int getCount();

}
