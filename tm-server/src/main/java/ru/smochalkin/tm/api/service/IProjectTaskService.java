package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.TaskDto;

import java.util.List;

public interface IProjectTaskService {

    void bindTaskByProjectId(@Nullable String projectId, @Nullable String taskId);

    void bindTaskByProjectId(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskByProjectId(@Nullable String projectId, @Nullable String taskId);

    void unbindTaskByProjectId(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    List<TaskDto> findTasksByProjectId(@Nullable String projectId);

    @NotNull
    List<TaskDto> findTasksByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeProjectById(@Nullable String projectId);

    void removeProjectByName(@Nullable String userId, @Nullable String name);

    void removeProjectByIndex(@Nullable String userId, @NotNull Integer index);

}
