package ru.smochalkin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public final class TaskDto extends AbstractBusinessEntityDto {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

}
