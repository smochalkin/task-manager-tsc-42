package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.dto.AbstractEntityDto;

public abstract class AbstractService<E extends AbstractEntityDto> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
