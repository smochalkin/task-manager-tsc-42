package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.dto.TaskDto;

import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(@Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.bindTaskById(projectId, taskId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.bindTaskByUserId(userId, projectId, taskId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskByProjectId(@Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.unbindTaskById(projectId, taskId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.unbindTaskByUserId(userId, projectId, taskId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findTasksByProjectId(@Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findTasksByProjectId(projectId);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(userId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findTasksByUserIdAndProjectId(userId, projectId);
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
            @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
            try {
                taskRepository.removeTasksByProjectId(id);
                projectRepository.removeById(id);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
            ProjectDto projectDto = projectRepository.findByName(userId, name);
            removeProjectById(projectDto.getId());
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (isEmpty(userId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
            ProjectDto projectDto = projectRepository.findByIndex(userId, index);
            removeProjectById(projectDto.getId());
        }
    }

}
