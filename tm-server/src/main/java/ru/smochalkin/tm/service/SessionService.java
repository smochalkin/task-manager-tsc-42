package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.ISessionRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.ISessionService;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.empty.EmptyPasswordException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.UserIsLocked;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.util.HashUtil;

import java.util.List;
import java.util.UUID;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public class SessionService extends AbstractService<SessionDto> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(
            @NotNull IConnectionService connectionService,
            @NotNull ServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public void clear() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.clear();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<SessionDto> findAll() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.findAll();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public SessionDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.findById(id);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.removeById(id);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public int getCount() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.getCount();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public final SessionDto open(@NotNull final String login, @NotNull final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final UserDto userDto = serviceLocator.getUserService().findByLogin(login);
        if (userDto.isLock()) throw new UserIsLocked();
        @NotNull final String secret = serviceLocator.getPropertyService().getPasswordSecret();
        @NotNull final Integer iteration = serviceLocator.getPropertyService().getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(password, secret, iteration);
        if (!userDto.getPasswordHash().equals(hash)) throw new AccessDeniedException();
        @NotNull final SessionDto sessionDto = new SessionDto(userDto.getId());
        sign(sessionDto);
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.add(
                        UUID.randomUUID().toString(),
                        userDto.getId(),
                        sessionDto.getSignature(),
                        sessionDto.getTimestamp()
                );
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
        return sessionDto;
    }

    @Override
    @SneakyThrows
    public final void close(@NotNull final SessionDto sessionDto) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.removeById(sessionDto.getId());
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public final void closeAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.removeByUserId(userId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public final void validate(@Nullable final SessionDto sessionDto) {
        if (sessionDto == null) throw new AccessDeniedException();
        if (isEmpty(sessionDto.getSignature())) throw new AccessDeniedException();
        if (isEmpty(sessionDto.getUserId())) throw new AccessDeniedException();
        @Nullable final SessionDto temp = sessionDto.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = sessionDto.getSignature();
        @Nullable final SessionDto sessionDtoTarget = sign(temp);
        @Nullable final String signatureTarget = sessionDtoTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try{
                repository.findById(sessionDto.getId());
            }catch (Exception e){
                throw new AccessDeniedException();
            }
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDto sessionDto, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(sessionDto);
        final @NotNull UserDto userDto = serviceLocator.getUserService().findById(sessionDto.getUserId());
        if (!userDto.getRole().equals(role)) throw new AccessDeniedException();
    }

    @NotNull
    public SessionDto sign(@NotNull final SessionDto sessionDto) {
        sessionDto.setSignature(null);
        @NotNull final IPropertyService ps = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(sessionDto, ps.getSignSecret(), ps.getSignIteration());
        sessionDto.setSignature(signature);
        return sessionDto;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<SessionDto> findAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

}