package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.dto.TaskDto;

import java.util.*;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class TaskService extends AbstractBusinessService<TaskDto> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findAll();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findById(id);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.removeById(id);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public int getCount() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.getCount();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.clear();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.clearByUserId(userId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll(@NotNull final String userId) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll(@NotNull final String userId, @NotNull final String sort) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findAllByUserIdSorted(userId, sort);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskDto findById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findByUserIdAndId(userId, id);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskDto findByName(@NotNull final String userId, @NotNull final String name) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findByName(userId, name);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskDto findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findByIndex(userId, index);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.removeByUserIdAndId(userId, id);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.removeByName(userId, name);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.removeByIndex(userId, index);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(@NotNull final String id, @NotNull final String name, @Nullable final String desc) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.updateById(id, name, desc);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.updateByUserIdAndId(userId, id, name, desc);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.updateByIndex(userId, index, name, desc);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String strStatus
    ) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            Status status = Status.getStatus(strStatus);
            Map<String, Date> dateMap = prepareDates(status);
            try {
                repository.updateStatusById(userId, id, status, dateMap.get("start_date"), dateMap.get("end_date"));
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String strStatus
    ) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            Status status = Status.getStatus(strStatus);
            Map<String, Date> dateMap = prepareDates(status);
            try {
                repository.updateStatusByName(userId, name, status, dateMap.get("start_date"), dateMap.get("end_date"));
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String strStatus
    ) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            Status status = Status.getStatus(strStatus);
            Map<String, Date> dateMap = prepareDates(status);
            try {
                repository.updateStatusByIndex(userId, index, status, dateMap.get("start_date"), dateMap.get("end_date"));
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public boolean isNotIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) return true;
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return index >= repository.getCountByUser(userId);
        }
    }

    @Override
    @SneakyThrows
    public void create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            try {
                repository.add(
                        UUID.randomUUID().toString(),
                        name,
                        description,
                        Status.NOT_STARTED.name(),
                        userId
                );
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @SneakyThrows
    private Map<String, Date> prepareDates(@NotNull final Status status) {
        Map<String, Date> map = new HashMap<>();
        switch (status) {
            case IN_PROGRESS:
                map.put("start_date", new Date());
                map.put("end_date", null);
                break;
            case COMPLETED:
                map.put("start_date", null);
                map.put("end_date", new Date());
                break;
            case NOT_STARTED:
                map.put("start_date", null);
                map.put("end_date", null);
        }
        return map;
    }

}
