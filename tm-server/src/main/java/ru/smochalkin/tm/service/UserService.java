package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.*;
import ru.smochalkin.tm.exception.system.LoginExistsException;
import ru.smochalkin.tm.util.HashUtil;

import java.util.List;
import java.util.UUID;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class UserService extends AbstractService<UserDto> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull IPropertyService propertyService,
            @NotNull IConnectionService connectionService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public void clear() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            try {
                repository.clear();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<UserDto> findAll() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            return repository.findAll();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            return repository.findById(id);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            try {
                repository.removeById(id);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public int getCount() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            return repository.getCount();
        }
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            try {
                repository.add(UUID.randomUUID().toString(), login, getPasswordHash(password), email, Role.USER);
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserDto findByLogin(@NotNull final String login) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            return repository.findByLogin(login);
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@NotNull final String login) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            try {
                repository.removeByLogin(login);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            try {
                repository.updatePasswordById(id, getPasswordHash(password));
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            try {
                repository.updateById(id, firstName, lastName, middleName);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            @Nullable final UserDto userDto = repository.findByLogin(login);
            return userDto != null;
        }
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            try {
                repository.updateLockByLogin(login, true);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            try {
                repository.updateLockByLogin(login, false);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @NotNull
    private String getPasswordHash(@NotNull final String password) {
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        return HashUtil.salt(password, secret, iteration);
    }

}

