package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.component.Bootstrap;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.UserDto;

import java.util.List;

public class SessionServiceTest {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    private int sessionCount;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        userService = new UserService(propertyService, connectionService);
        sessionService = new SessionService(connectionService, new Bootstrap());
        userService.create("test1", "1","@");
        userService.create("test2", "2","@");
        sessionService.open("test1","1");
        sessionCount = sessionService.getCount();
    }

    @After
    public void end() {
        @NotNull final UserDto userDto1 = userService.findByLogin("test1");
        @NotNull final UserDto userDto2 = userService.findByLogin("test2");
        sessionService.closeAllByUserId(userDto1.getId());
        sessionService.closeAllByUserId(userDto2.getId());
        userService.removeByLogin("test1");
        userService.removeByLogin("test2");
    }

    @Test
    public void openTest() {
        sessionService.open("test2", "2");
        Assert.assertEquals(sessionCount + 1, sessionService.getCount());
    }

    @Test
    public void closeTest() {
        @NotNull final UserDto userDto = userService.findByLogin("test1");
        @NotNull final SessionDto sessionDto = sessionService.findAllByUserId(userDto.getId()).get(0);
        sessionService.close(sessionDto);
        Assert.assertEquals(sessionCount - 1, sessionService.getCount());
    }

    @Test
    public void closeAllByUserIdTest() {
        @NotNull final String userId = userService.findByLogin("test1").getId();
        @NotNull final List<SessionDto> sessionDtos = sessionService.findAllByUserId(userId);
        @NotNull final int resultSize = sessionService.getCount() - sessionDtos.size();
        sessionService.closeAllByUserId(userId);
        Assert.assertEquals(resultSize, sessionService.getCount());
    }

    @Test
    public void validateTest() {
        @NotNull final SessionDto sessionDto = sessionService.open("test1", "1");
        sessionDto.setSignature(null);
        Assert.assertThrows(AccessDeniedException.class,
                () -> sessionService.validate(sessionDto));
    }

    @Test
    public void validateRoleTest() {
        @NotNull final SessionDto sessionDto = sessionService.open("test1", "1");
        Assert.assertThrows(AccessDeniedException.class,
                () -> sessionService.validate(sessionDto, Role.ADMIN));
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final String userId = sessionService.findAll().get(0).getUserId();
        @NotNull final List<SessionDto> sessionDtos = sessionService.findAllByUserId(userId);
        Assert.assertEquals(userId, sessionDtos.get(0).getUserId());
    }

}
